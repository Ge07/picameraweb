# PiCameraWeb

## Bienvenue sur le projet PicameraWeb !

Je me suis mis à utiliser le super script python PiCamera pour utiliser le camera HQ de raspberry pi en tant que camera de microscope pour mon trinoculaire. Je suis aussi astrophotographe amateur, il y aura probablement des développements de fonctionnalités spécifiques à ces deux applications. Les scripts sont puissants, mais je ne suis pas fan de leur usage au quotidien, aussi, les câbles sont une contrainte insupportable ! J'ai donc décidé d'apprendre à programmer en python en en développant une interface web pour le script PiCamera, peut-être n'est-ce pas la manière la plus simple de s'y prendre, mais, après tou ! Pourquoi pas apprendre en développant queqlque-chose d'utile ?

Comme décrit dans la licence, le projet est soumis aux termes de la licence **CC - BY - SA 4.0** , vous êtres libres de "forquer" pour tout usage que vous voudriez en faire, y compris commercial, du moment que vous respectez les clauses de la licence (*vous avez les détails de ce que vous pouvez faire ici : https://creativecommons.org/licenses/by-sa/4.0/*). Je vous serais cependant très reconnaissant de votre aide, vos avis et que vous "commitiez" à cette version, j'ai beaucoup à apprendre de vous.

J'ai eu pas mal de frais pour tertes différents matériels (et je suis disponnible pour échanger avec vous sur ce thème également) et ne demande pas (pour l'instant ?) de participation financière (qui sera toujours sur la base du volontartiat !), mais je vous serais très reconnaissant de soutenir l'association FRAMASOFT si vous aimez mon travail et voulez m'encourager à continuer , vous pouvez suivre ce lien : https://soutenir.framasoft.org/fr/

## Welcome to PiCAmeraWeb project !

I intended to use the great PiCamera python script for using raspberry pi camera HQ as a microscope camera for my trinocular. As I'm also an enthusiastic astrophotographer, there will probably be some specific functionalities developed for both of these applications. Scripts are powerful, but I'm not fond of it on production use, also, cables could be a mess ! Hence, I decided to learn python programming by developing a web interface for PiCamera script, maybe not the simpler way to start, but, hey ! Why not learning by developing something useful ?

As stated by the licence, feel free to fork this job for any use, including commercial ones as long as you respect the **CC - BY - SA 4.0** terms (*see here for details : https://creativecommons.org/licenses/by-sa/4.0/*). I would anyway be very thankful for your help, advises and commits in this version, I got a lot to learn !

I had a ton of expanses for material testing (feel free to discuss about this subject too) but am not (yet ?) asking for any money but feel free to help FRAMASOFT association by following this link if you like the job and want to support it : https://soutenir.framasoft.org/en/?f=nav
